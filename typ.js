//
// typ(o, typesToCheck='')
// 
// Javascript type checking function 
// 
// By Runsun Pan (runsun at gmail dot com)
// MIT 2016 Runsun Pan
//


function typ(o, typesToCheck='')
{
  let typ
  
  if(o+''=='NaN'){ typ = 'NaN' }
  else if(typeof(o)!='string' 
          && (o+'').indexOf('class ')==0){ typ = 'Class' }
  else
  try
  { 
    // This approach can't deal with null but can handle class
    typ= o.constructor.toString(
           ).split(/[\(\{]/)[0].split(' ')[1]
    
  } 
  catch(e) 
  {  
    // This approach, described by Todd Motto in 
    // https://toddmotto.com/understanding-javascript-types-and-reliable-type-checking/
    // can't deal with class
    typ= Object.prototype.toString.call(o).slice(8, -1) 
  }
  
  // Divide Number to Integer or Float
  typ = typ=='Number'?
              ( parseInt(o,10)==o?'Integer':'Float')
               :typ
  
  //console.log('typesToCheck= ', typesToCheck)
  if( typesToCheck )
  {
     typesToCheck= typeof(typesToCheck)=='string'?
                   typesToCheck.split(/[ ,]/): typesToCheck
     typ= typesToCheck.filter( x=> x==typ ).length>0
  }  
  return typ
}  

typ.info ={ 
  name: 'typ'
  ,  doc: ['']
  , tests: [ 
    
     ''
    ,'<b>Javascript type checking function</b>'
    ,''
    ,`t> typ( 'abc' ) := String`
    ,`t> typ( [1,2] ) := Array`
    ,`t> typ( false ) := Boolean`
    ,`t> typ( /abc/ ) := RegExp`
    ,`t> typ( x=> x ) := Function`
    ,`a> f= ()=> (x)=>x`
    ,`t> typ( f() ) := Function`
    
    ,''
    ,`t> typ( 3.3 ) := Float`
    ,`t> typ( .3 ) := Float`
    ,`t> typ( 3 ) := Integer`
    ,[`t> typ( 3.0 ) := Integer`,{cmt:'Note this'}]
    ,''
    ,`t> typ( new Date() ) := Date`
    ,`t> typ(new Error()) := Error`
    ,''
    ,`t> typ(null) := Null`
    ,`t> typ(undefined) := Undefined`
    ,`t> typ(NaN) := NaN`
    ,`t> typ( new Number("abc") ) := NaN`
    ,''
    ,'<hr/><b>typesToCheck</b>:'
    ,''
    ,'This argument allows for checking if the target( o )'
    ,'is among the typesToCheck. It could be one of the following:'
    ,''
    ,' 1. string w/ type names seperated by a space'
    ,' 2. string w/ type names seperated by a comma'
    ,' 3. array containing type names'
    ,''
    ,`t> typ( [1,2], 'String Array') := true`
    ,`t> typ( [1,2], 'String, Integer') := false`
    ,`t> typ( [1,2], ['String', 'Array']) := true`
    ,`t> typ( [1,2], ['String', 'Integer']) := false`
    ,''
    ,'<hr/><b>Class</b>:'
    ,''
    ,`a> MyCls= class MyCls{ constructor(){} }`
    ,''
    ,'// A class gets "Class":'
    ,''
    ,`t> typeof( MyCls ) := function`
    ,`t> typ( MyCls ) := Class`
    ,''
    ,'// A class instance gets the class name:'
    ,''
    ,`a> myc = new MyCls()`
    ,`t> typeof( myc ) := object`
    ,`t> typ( myc ) := MyCls`
    
    ,''
    ,`a> HisCls= class HisCls{ constructor(){} }`
    ,`a> hisc = new HisCls()`
    ,`t> typ( hisc, 'MyCls, Array') := false`
    ,`t> typ( hisc, 'MyCls, HisCls') := true`
    ,''
    ,`a> KidCls = (class KidCls extends MyCls{ constructor(){super()} })`
    ,''
    ,`t> typeof( KidCls ) := function`
    ,`t> typ( KidCls ) := Class`
    ,''
    ,`a> kid = new KidCls()`
    ,`t> typeof( kid ) := object`
    ,`t> typ( kid ) := KidCls`
    ,''
    ,'<hr/>'
    ,`t> ( ()=>typ(arguments) )() := Object`
    ,`t> ( (...args)=>typ(args) )() := Array`
    
   ]
}     

function log_(s, data, deco)
{
   console.log( s_(s,data,deco) )
}  

function doctest() 
{
  // need: jxx and jQuery 
  // http://runsun.info/prog/js/proj/jxx/jxx.js

  function test() 
  {  
    testfuncs = [ typ
              ]
    var out
    out = testfuncs.map(x=> jx.test.func(x))
    document.body.innerHTML += out.join('')
  }  

  $.get('https://bitbucket.org/runsun/jxlog/raw/193083aae6fd268d0844e98cad8cec4137781c34/jxlog.js'
     , (data)=>
      {
        eval(data)
        window.log=jxlog
        test()
      } 
  )
}  

doctest()
